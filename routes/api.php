<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
*/

$api->group(['prefix' => 'docs'], function($api) {
    $api->get('/', [
        'as' => 'docs',
        'uses' => 'Doc\DocumentationController@get'
    ]);
});

$api->group(['prefix' => 'auth'], function($api) {
    $api->post('/register', [
        'as' => 'auth.create', 
        'uses' => 'Auth\AuthController@register', 
        'middleware' => []
    ]);

    $api->post('/login', [
        'as' => 'auth.login', 
        'uses' => 'Auth\AuthController@authenticate', 
        'middleware' => []
    ]);
});

$api->group(['prefix' => 'user', 'middleware' => ['jwt.auth']], function ($api) {
    $api->get('/', [
        'as' => 'user.index',
        'uses' => 'User\UserController@index',
    ]);
    $api->get('/{id}', [
        'as' => 'user.get',
        'uses' => 'User\UserController@get'
    ]);   
});

