<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
		DB::table('roles')->truncate();
		DB::table('role_users')->truncate();

		$adminRole = [
			'name' => 'Admin',
			'slug' => 'admin',
			'permissions' => [
				'admin' => true,
			]
		];
		

		$userRole = [
			'name' => 'User',
			'slug' => 'user',
            'permissions' => [
                'user.view' => true,
                'user.update' => true,
                'user.create' => true,
                'user.delete' => true
            ]
		];
        
        $idAdmin = Sentinel::getRoleRepository()->createModel()->fill($adminRole)->save();
		$idUser = Sentinel::getRoleRepository()->createModel()->fill($userRole)->save();
        
        $adminUserData = [
			'email'    => 'admin@example.com',
			'password' => 'password',
		];
		
        $adminUser = Sentinel::registerAndActivate($adminUserData);
        $adminUser->roles()->attach(Sentinel::findRoleByName('Admin'));
        $adminUser->roles()->attach(Sentinel::findRoleByName('User'));
        
    }
}
