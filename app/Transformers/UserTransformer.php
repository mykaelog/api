<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use Cartalyst\Sentinel\Users\EloquentUser;

class UserTransformer extends TransformerAbstract {

    /**
     * Transform resource into standard output format with correct typing
     * @param Package $package  Resource being transformed
     * @return array              Transformed object array ready for output
     */
    public function transform(EloquentUser $user)
    {
        return [
            'id'            => (int) $user->id,
            'email'         => $user->email,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'permissions'   => $user->permissions,
            'last_login'    => $user->last_login,
            'created_at'    => $user->created_at,
            'roles'         => $user->roles
        ];
    }
}