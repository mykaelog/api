<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends EloquentUser implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    //use Notifiable;
     use Notifiable, Authenticatable, Authorizable, CanResetPassword;
    
    public function isAdmin()
    {
        return $this->hasAccess(['admin']);
    }

    public function hasAccessTo(array $permissions)
    {
        array_push($permissions, 'admin');

        return $this->hasAccess($permissions);
    }

    public function hasAnyAccessTo(array $permissions)
    {
        array_push($permissions, 'admin');

        return $this->hasAnyAccess($permissions);
    }
    
}
