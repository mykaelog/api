<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;
use Dingo\Api\Exception\StoreResourceFailedException;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            config('api.credentialFields.email')      => 'required|unique:users|max:255|email',
            config('api.credentialFields.password')   => 'required|alpha_num|min:6',
        ];
    }


    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            throw new StoreResourceFailedException('cannot_create_user', $errors);
        }
    }
    
}
