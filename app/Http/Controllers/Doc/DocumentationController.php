<?php

namespace App\Http\Controllers\Doc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use File;
use Artisan;

class DocumentationController extends Controller
{
    const API_FILE = 'ApiDoc.md';


    public function get()
    {
        $filePath = sprintf('%s/%s/%s', public_path(), 'doc', self::API_FILE);
        
        try
        {
            $file = File::get($filePath);

        }
        catch(FileNotFoundException $exception)
        {
            //api:docs --name Example --use-version v2 --output-file /path/to/documentation.md
            Artisan::call('api:docs', [
                '--name'        => self::API_FILE,
                '--use-version' => 'v1',
                '--output-file' => $filePath
            ]);
        }
        
        return response()->download($filePath);
    }
}
