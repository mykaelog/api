<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use JWTAuth;

/**
 * Users Resource
 * @Resource("Users", uri="/users")
 */
class UserController extends Controller
{
    
    /**
    * Show all users
    *
    * Get a JSON representation of all the registered users.
    * Only if current user has role Admin.
    *
    * @Get("/")
    * @Versions({"v1"})
    *   @Response(200, body={{
    *       "id": 10, "email": "foo@gmail.com", 
    *       "first_name" : "foo", "last_name" : "bar",
    *       "last_login" : "DateTime", "created_at" : "DateTime",
    *       "roles" : "array"
    *       }})
    *   })
    *   @Response(403, body={"message": {"Forbidden"}})
    */
    public function index()
    {
        $auth = JWTAuth::parseToken()->authenticate();
        
        if( !$auth->isAdmin())
        {
             return $this->response
                ->errorForbidden();
        }
        
        $users = User::with('roles')->get();

        return $this->response
            ->collection($users, new UserTransformer)
            ->setStatusCode(200);
    }

    /**
    * Show user by Id.
    *
    * Get a JSON representation of registered user Id.
    * If current user is himself or has admin role.
    *
    * @Get("/{id}")
    * @Versions({"v1"})
    * @Parameters({
    *      @Parameter("id", type="integer", required=true, description="The user Id.")
    * })
    * @Transaction({
    *   @Request({"id": "10"}),
    *   @Response(200, body={
    *       "id": 10, "email": "foo@gmail.com", 
    *       "first_name" : "foo", "last_name" : "bar",
    *       "last_login" : "DateTime", "created_at" : "DateTime",
    *       "roles" : "array"
    *       })
    *   }),
    *   @Response(403, body={"message": {"Forbidden"}})
    *   @Response(422, body={"message": {"Not Found"}})
    * })
    */
    public function get($id)
    {
        try
        {
            $auth = JWTAuth::parseToken()->authenticate();
            $user = User::with('roles')->findOrFail($id);
            if($auth->cant('view', $user))
            {
                return $this->response
                    ->errorForbidden();
            }
        }
        catch(ModelNotFoundException $exception)
        {
            return $this->response
                ->errorNotFound();
        }

        return $this->response
            ->item($user, new UserTransformer)
            ->setStatusCode(200);
    }

    public function update(Request $request)
    {

    }

    public function delete(Request $request)
    {

    }
}
