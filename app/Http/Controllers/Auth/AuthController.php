<?php

namespace App\Http\Controllers\Auth;


use JWTAuth;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\Http\Requests\RegistrationRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Database\QueryException;

/**
 * Authentication Resource
 * @Resource("Auth", uri="/auth")
 */
class AuthController extends Controller
{

    /**
    * Try to register new User on application
    *
    * Get a JSON representation of all the registered users.
    *
    * @Post("/register")
    * @Versions({"v1"})
    * @Parameters({
    *      @Parameter("email", type="char", required=true, description="The user Email address."),
    *      @Parameter("password", type="char", required=true, description="The user password.")
    * })
    * @Transaction({
    *   @Request({"email": "foo@gmail.com", "password" : "123456"}),
    *   @Response(201, body={}),
    *   @Response(422, body={"message": {"cannot_create_user"}, "errors" : "[]"}),
    *   @Response(500, body={"message": {"register_error"}})
    * })
    */
    public function register(RegistrationRequest $request)
    {
        $credentials = $request->only(
            config('api.credentialFields.email'),
            config('api.credentialFields.password')
        );

        
        if( !$user = Sentinel::register($credentials, true))
        {
            return $this->response
                ->array([
                    'message' => 'register_error'
                ])
                ->setStatusCode(500);
        }
        
        return $this->response
            ->created(route('user.get', ['id' => $user->id]));
            
    }

    /**
    * Try to authenticate an existing User on application
    *
    * Get a JSON representation with JWT (Json Web Token).
    *
    * @Post("/login")
    * @Versions({"v1"})
    * @Parameters({
    *      @Parameter("email", type="char", required=true, description="The user Email address."),
    *      @Parameter("password", type="char", required=true, description="The user password.")
    * })
    * @Transaction({
    *   @Request({"email": "foo@gmail.com", "password" : "123456"}),
    *   @Response(200, body={"token" : "token_code_response"}),
    *   @Response(401, body={"message": {"invalid_credentials"}}),
    *   @Response(500, body={"message": {"could_not_create_token"}})
    * })
    */
    public function authenticate(Request $request)
    {
        $credentials = $request->only(
            config('api.credentialFields.email'),
            config('api.credentialFields.password')
        );

        try
        {
            if( !$token = JWTAuth::attempt($credentials))
            {
                return $this->response
                    ->array([
                        'message' => 'invalid_credentials'
                    ])
                    ->setStatusCode(401);
            }
        }
        catch(JWTException $exception)
        {
            return $this->response
                ->array([
                    'message' => 'could_not_create_token']
                )
                ->setStatusCode(500);
        }

        return $this->response
            ->array(compact('token'))
            ->setStatusCode(200);
    }
}
