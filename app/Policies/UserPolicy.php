<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Before Hook determine if current user is in Admin Role
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    /*public function before(User $user, $ability)
    {
        return $user->isAdmin();
    }*/

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $current, User $user)
    {
        return $current->id == $user->id || $current->isAdmin();
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user, User $user)
    {
        return $user->id == $id || $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user, User $user)
    {
        return $user->id == $id || $user->isAdmin();
    }
}
